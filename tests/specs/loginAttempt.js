var assert = require('assert');
const { config } = require('../../conf/thameside.conf');

describe('Login Attempt', () => {

  it('Login Attempt Invalid Password', async () => {
    console.log('Login Attempt Invalid Password');

    var platform=config.capabilities[0].platform;
    console.log("Platofrm  : "+ platform);
    await browser.pause(20000);

    if(platform !='iOS'){
      console.log('Execution is in progress for Android device');
      var skipButton = await $(`~skip_button_0`) 
      await skipButton.waitForDisplayed({ timeout: 10000 });
      await skipButton.click();
    }
    else{
      console.log('Execution is in progress for iOS device');
      var skipButton = await $$('//*[@name="skip_button_0"]'); 
      console.log(skipButton.length);
      await skipButton[1].waitForDisplayed({ timeout: 10000 });
      await skipButton[1].click();
    }
    await browser.pause(10000);

    var email = await $(`~login_email_textfield`);
    await email.waitForDisplayed({ timeout: 10000 });
    await email.setValue('inValid');
    await browser.pause(5000);

    var password = await $(`~login_password_textfield`);
    await password.waitForDisplayed({ timeout: 10000 });
    await password.setValue('pass');
    await browser.pause(5000);

    var loginButton = await $(`~login_button`);
    await loginButton.waitForDisplayed({ timeout: 10000 });
    await loginButton.click();
    
    console.log("Login Attempt - Invalid User Credentials");

    var error = await $('android=new UiSelector().text("Error")');
    await error.waitForDisplayed({ timeout: 10000 });

    var errorMessage = await $('android=new UiSelector().text("The username must be a valid email address.")');
    await errorMessage.waitForDisplayed({ timeout: 10000 });

    });
});
