var assert = require('assert');
const { Assertion } = require('chai');
const { config } = require('../../conf/thameside.conf');

describe('Security Settings', () => {
  it('Security Settings', async () => {
    var platform=config.capabilities[0].platform;
    console.log("Platofrm  : "+ platform);
    await browser.pause(20000);

    if(platform !='iOS'){
      console.log('Execution is in progress for Android device');
      var skipButton = await $(`~skip_button_0`) 
      await skipButton.waitForDisplayed({ timeout: 10000 });
      await skipButton.click();
    }
    else{
      console.log('Execution is in progress for iOS device');
      var skipButton = await $$('//*[@name="skip_button_0"]'); 
      console.log(skipButton.length);
      await skipButton[1].waitForDisplayed({ timeout: 10000 });
      await skipButton[1].click();
    }
    
    await browser.pause(10000);

    var email = await $(`~login_email_textfield`);
    await email.waitForDisplayed({ timeout: 10000 });
    await email.setValue('testCustomer@example.com');
    await browser.pause(5000);

    var password = await $(`~login_password_textfield`);
    await password.waitForDisplayed({ timeout: 10000 });
    await password.setValue('password1');
    await browser.pause(5000);

    var loginButton = await $(`~login_button`);
    await loginButton.waitForDisplayed({ timeout: 10000 });
    await loginButton.click();
    await browser.pause(8000);
  
    if(platform =='iOS'){
      var noThanksButton = await $(`~no_thanks_button`);
      await noThanksButton.waitForDisplayed({ timeout: 10000 });
      await noThanksButton.click();
      await browser.pause(10000);
    }

    var createPin = await $(`~create_pin_input`);
    await createPin.waitForDisplayed({ timeout: 10000 });
    await createPin.setValue('9864');

    var next = await $(`~create_pin_next`);
    await next.waitForDisplayed({ timeout: 10000 });
    await next.click();
    await browser.pause(8000);

    var retypePin = await $(`~retype_pin_input`);
    await retypePin.waitForDisplayed({ timeout: 10000 });
    await retypePin.setValue('9864');

    var next2 = await $(`~retype_pin_next`);
    await next2.waitForDisplayed({ timeout: 10000 });
    await next2.click();
    await browser.pause(20000);
    
    console.log("logged in Successfully");

    var moreOption = await $(`~navigation_More`);
    await moreOption.waitForDisplayed({ timeout: 10000 });
    await moreOption.click();
    await browser.pause(10000);

    console.log("Security Settings");
    var settings = await $(`~more_settings`);
    await settings.waitForDisplayed({ timeout: 10000 });

    var value=(await settings.getText()).toString();
    console.log("Settings GetText Details : "+value);
    await settings.click();
    await browser.pause(10000);
    
    var loginSecurity = await $(`~login_security`);
    await loginSecurity.waitForDisplayed({ timeout: 10000 });
    await loginSecurity.click();
    await browser.pause(8000);

    var existing_password_input = await $(`~existing_password_input`);
    await existing_password_input.waitForDisplayed({ timeout: 10000 });
    assert(existing_password_input.isDisplayed());

    var new_password_input = await $(`~new_password_input`);
    await new_password_input.waitForDisplayed({ timeout: 10000 });
    assert(new_password_input.isDisplayed());

    var twoFactorLogin = await $(`~two_factor_switch`);
    assert(twoFactorLogin.isDisplayed());

  });
});



