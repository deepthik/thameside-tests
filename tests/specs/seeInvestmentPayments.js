var assert = require('assert');
const { Assertion } = require('chai');
const { config } = require('../../conf/thameside.conf');

describe('See Investment Payments screen', () => {
  it('See Investment Payments screen', async () => {
    var platform=config.capabilities[0].platform;
    console.log("Platofrm  : "+ platform);
    await browser.pause(20000);

    if(platform !='iOS'){
      console.log('Execution is in progress for Android device');
      var skipButton = await $(`~skip_button_0`) 
      await skipButton.waitForDisplayed({ timeout: 10000 });
      await skipButton.click();
    }
    else{
      console.log('Execution is in progress for iOS device');
      var skipButton = await $$('//*[@name="skip_button_0"]'); 
      console.log(skipButton.length);
      await skipButton[1].waitForDisplayed({ timeout: 10000 });
      await skipButton[1].click();
    }
    
    await browser.pause(10000);

    var email = await $(`~login_email_textfield`);
    await email.waitForDisplayed({ timeout: 10000 });
    await email.setValue('testCustomer@example.com');
    await browser.pause(5000);

    var password = await $(`~login_password_textfield`);
    await password.waitForDisplayed({ timeout: 10000 });
    await password.setValue('password1');
    await browser.pause(5000);

    var loginButton = await $(`~login_button`);
    await loginButton.waitForDisplayed({ timeout: 10000 });
    await loginButton.click();
    await browser.pause(8000);
  
    if(platform =='iOS'){
      var noThanksButton = await $(`~no_thanks_button`);
      await noThanksButton.waitForDisplayed({ timeout: 10000 });
      await noThanksButton.click();
      await browser.pause(10000);
    }

    var createPin = await $(`~create_pin_input`);
    await createPin.waitForDisplayed({ timeout: 10000 });
    await createPin.setValue('9864');

    var next = await $(`~create_pin_next`);
    await next.waitForDisplayed({ timeout: 10000 });
    await next.click();
    await browser.pause(8000);

    var retypePin = await $(`~retype_pin_input`);
    await retypePin.waitForDisplayed({ timeout: 10000 });
    await retypePin.setValue('9864');

    var next2 = await $(`~retype_pin_next`);
    await next2.waitForDisplayed({ timeout: 10000 });
    await next2.click();
    await browser.pause(20000);
    
    console.log("logged in Successfully");

    console.log("See Investment Payment");
    //Home Page Verification
    var homePage = await $(`~active_accounts_title`);
    await homePage.waitForDisplayed({ timeout: 10000 });
   // var title=(await homePage.getText()).toString();
   // assert(title=='Accounts.', title);
    await browser.pause(10000);
    console.log("My Investments Summary (Home Page)");

    // My Investment Page Verification
    var account = await $(`~account_card_0`);
    await account.waitForDisplayed({ timeout: 10000 });
    await account.click();
    await browser.pause(20000);

    var accountDetails = await $(`~acount_details_title`);
    await accountDetails.waitForDisplayed({ timeout: 10000 });
   // console.log((await accountDetails.getText()).toString());
    assert(accountDetails.isDisplayed)
    await browser.pause(20000);

    var detailsCard = await $(`~details_card_3`);
    await detailsCard.waitForDisplayed({ timeout: 10000 });
  //  console.log((await detailsCard.getText()).toString());
    await detailsCard.click();
    await browser.pause(20000);
       
    var topUp = await $(`~top_up`);
    await topUp.waitForDisplayed({ timeout: 10000 });
    var topUpDisplayed=topUp.isDisplayed();

    console.log(Boolean(topUpDisplayed));
    if(Boolean(topUpDisplayed)){
      assert(true);
    }else{
      assert(false);
    }
    assert(topUp.isDisplayed());

    var withdrawals = await $(`~withdraw_funds`);
    await withdrawals.waitForDisplayed({ timeout: 10000 });
    assert(withdrawals.isDisplayed());

    await browser.pause(8000);
    console.log('Investment Page verification completed');
  });
});



