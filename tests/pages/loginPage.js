class LoginPage {

    validLogin(){
        await browser.pause(20000);
        console.log('Login Success');
        
        var skipButton = await $(`~skip_button`) 
        await skipButton.waitForDisplayed({ timeout: 10000 });
        await skipButton.click();
        
        await browser.pause(20000);
    
        var email = await $(`~email_textfield`);
        await email.waitForDisplayed({ timeout: 10000 });
        await email.setValue('testCustomer@example.com');
        await browser.pause(5000);
    
        var password = await $(`~password_textfield`);
        await password.waitForDisplayed({ timeout: 10000 });
        await password.setValue('password1');
        await browser.pause(5000);
    
        var loginButton=await $$('//android.view.ViewGroup/android.widget.TextView[@text="Log in"]')
        console.log('Matched Element size : '+ loginButton.length );
        await browser.pause(5000);
        await loginButton[1].click();
        await browser.pause(8000);
    }

}

export default new LoginPage();
