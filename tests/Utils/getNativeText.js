/**
 * This a method to get text from a native iOS/Android app component.
 * It can be used when you are using webdriver.io
 *
 * In the app I need to automate I found out that:
 *  - iOS will have the complete text (of all the childeren) in the parent component.
 *  - on Android a component can have mutiple childrens, that's why you get an array of text back  
 * 
 * Feel free to use it for all kinds of purposes, a star is much appreciated ;-)
 *
 * Grtz,
 * Wim | wswebreation
 */

/**
 * Get the text of a native Android / iOS element
 *
 * @param {element} element
 *
 * @return {string}
 */
export function getTextOfElement(element) {
    let visualText;
    try {
      visualText = element.getText(browser.isAndroid ? ANDROID_SELECTORS.TEXT : IOS_SELECTORS.GENERIC_TEXT);
    } catch (e) {
      visualText = element.getText();
    }
  
    if (typeof visualText === 'string') {
      return visualText;
    }
  
    return Array.isArray(visualText) ? visualText.join(' ') : '';
  }
  
  /**
   * Cross-platform selectors
   */
  const ANDROID_SELECTORS = {
    TEXT: '*//android.widget.TextView',
  };
  const IOS_SELECTORS = {
    GENERIC_TEXT: null,
  };