## Setup
* Clone the repo
* Install dependencies `npm install`
* You can setup environment variables for all sample repos (see Notes) or update `*.conf.js` files inside the `conf/` directory with your [BrowserStack Username and Access Key](https://www.browserstack.com/accounts/settings)
* For Example, open the thameside.config.js file, 
 
  - update the user and Key value in config file by taking the value from the browserstack accounts setting [(https://www.browserstack.com/accounts/settings)] 

  - Generate the application bs value by uploading the required .apk file or .iba file in [Generate BS Value](https://app-automate.browserstack.com/dashboard/v2/getting-started?source=%27home%27). In Step 2, upload the .apk or .ipa value in left hand side and take the bs value from the right hand slide once the file upload complete

  - update the above generated bs value in the config file

  - specfify the required tests inside the specs section

## Running your tests
- Open the root directory in gitbash
- Run the below command `./node_modules/.bin/wdio conf/thameside.conf.js`
- Test Results can be viewed in the [BrowserStack automate dashboard](https://app-automate.browserstack.com/dashboard/v2/builds/ed7b2a8c1fbaf4d169d2cff6d29be786a0cf9536)

## Additional Resources
* [Documentation for writing automate test scripts in Node](https://www.browserstack.com/automate/node)
* [Customizing your tests on BrowserStack](https://www.browserstack.com/automate/capabilities)
* [Browsers & mobile devices for selenium testing on BrowserStack](https://www.browserstack.com/list-of-browsers-and-platforms?product=automate)
* [Using REST API to access information about your tests via the command-line interface](https://www.browserstack.com/automate/rest-api)
