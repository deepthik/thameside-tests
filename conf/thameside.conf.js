exports.config = {
  user: process.env.BROWSERSTACK_USERNAME || 'BROWSERSTACK_USERNAME',
  key: process.env.BROWSERSTACK_ACCESS_KEY || 'BROWSERSTACK_ACC_KEY',
  updateJob: false,
  specs: [
    './tests/specs/viewTransactionDetails.js'
    /* './tests/specs/viewInvestmentPerformance.js',
        './tests/specs/viewTransactionDetails.js',
      './tests/specs/seeInvestmentPayments.js', 
         './tests/specs/myAccountViewPersonalDetails.js', 
         './tests/specs/myAccountViewPaymentDetails.js',
        './tests/specs/securitySettings.js',
   
       './tests/specs/loginSuccess.js', 
        './tests/specs/myInvestmentsSummary.js', 
           './tests/specs/viewInvestment.js',
       './tests/specs/loginAttempt.js' */
  ],
  exclude: [],

  capabilities: [
    
   {
    project: "Webdriverio iOS Project",
    build: 'Webdriverio iOS',
    name: 'iOS Test',
    platform:'iOS',
    device: 'iPhone SE',
    os_version: "11",
    app: process.env.BROWSERSTACK_APP_ID || 'bs://393bd9931d081b9d05c3208d4c64fd8cd9d5999d',
    'browserstack.debug': true
  } /*
  {
    automationName: 'Appium',
    project: "First Webdriverio Android Project",
    build: 'Webdriverio Android',
    name: 'Android Test',
    platform:'Android',
    device: 'Samsung Galaxy S8',
    os_version: "7.0",
    app: process.env.BROWSERSTACK_APP_ID || 'bs://698dfc636e9299736f4fe25bb43bdbb3071a6ec3',
    'browserstack.debug': true
  } */


],

  logLevel: 'warn',
  coloredLogs: true,
  screenshotPath: './errorShots/',
  baseUrl: '',
  waitforTimeout: 10000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  host: 'hub.browserstack.com',

  before: function () {
    var chai = require('chai');
    global.expect = chai.expect;
    chai.Should();
  },
  framework: 'mocha',
  mochaOpts: {
    ui: 'bdd',
    timeout: 1000000
  },

  // Code to mark the status of test on BrowserStack based on the assertion status
  afterTest: function (test, context, { error, result, duration, passed, retries }) {
    if(passed) {
      browser.executeScript('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed","reason": "Assertions passed"}}');
    } else {
      browser.executeScript('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed","reason": "At least 1 assertion failed"}}');
    }
  }
}
